package ru.itis.springfirstproject.models;

public enum Role {
    USER, ADMIN
}
