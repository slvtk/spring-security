package ru.itis.springfirstproject.models;

public enum State {
    NOT_CONFIRMED, CONFIRMED
}

