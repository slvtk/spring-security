package ru.itis.springfirstproject.models;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.File;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Slf4j
public class FileInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String storageFileName;
    private String originalFileName;
    private Long size;
    private String type;
    private String url;
    @Transient
    private String fileName;
    @Transient
    private File sourceFile;

    @OneToOne(cascade = {CascadeType.ALL})
    @Where(clause = "state=CONFIRMED")
    @JoinColumn(name = "user_id")
        private User user;

    @PostLoad
    public void loadFile(){
        sourceFile = new File(url);
        fileName = sourceFile.getName().substring(0, sourceFile.getName().lastIndexOf("."));
        log.info("Load file " + fileName);
    }
}


