package ru.itis.springfirstproject.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.springfirstproject.models.Test;
import ru.itis.springfirstproject.models.User;

import java.util.List;
import java.util.stream.Collectors;

    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Data
    public class TestDto {
        private Long id;
        private String title;
        private String description;
        private Integer complexity;
        public static TestDto from(Test test) {
            return TestDto.builder()
                    .id(test.getId())
                    .title(test.getTitle())
                    .description(test.getDescription())
                    .complexity(test.getComplexity())
                    .build();
        }

        public static List<TestDto> from(List<Test> tests) {
            return tests.stream()
                    .map(TestDto::from)
                    .collect(Collectors.toList());
        }
}
