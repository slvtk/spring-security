package ru.itis.springfirstproject.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.springfirstproject.models.Test;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TestsSearchResult {
    private List<Test> tests;
    private int pagesCount;
}

