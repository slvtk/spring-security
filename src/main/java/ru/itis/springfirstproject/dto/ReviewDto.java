package ru.itis.springfirstproject.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.springfirstproject.models.Review;
import ru.itis.springfirstproject.models.Test;
import ru.itis.springfirstproject.models.User;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ReviewDto {
    private Long id;
    private User user;
    private Test test;
    private String review;
    public static ReviewDto from(Review review) {
        return ReviewDto.builder()
                .id(review.getId())
                .user(review.getUser())
                .test(review.getTest())
                .review(review.getReview())
                .build();
    }

    public static List<ReviewDto> from(List<Review> reviews) {
        return reviews.stream()
                .map(ReviewDto::from)
                .collect(Collectors.toList());
    }
}
