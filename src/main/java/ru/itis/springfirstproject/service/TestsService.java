package ru.itis.springfirstproject.service;

import ru.itis.springfirstproject.dto.TestDto;
import ru.itis.springfirstproject.models.Test;

import java.util.List;

public interface TestsService {

    List<TestDto> getAllTests();

    Test getTestById(Long id);

    Integer checkAnswers(Integer[] array1, Integer[] array2);

}
