package ru.itis.springfirstproject.service;

import ru.itis.springfirstproject.models.User;

public interface SmsService {
    void sendSms(User user, String text);
}
