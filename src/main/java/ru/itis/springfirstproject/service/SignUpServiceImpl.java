package ru.itis.springfirstproject.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.itis.springfirstproject.dto.SignUpDto;
import ru.itis.springfirstproject.models.Role;
import ru.itis.springfirstproject.models.User;
import ru.itis.springfirstproject.repositories.UsersRepository;
import ru.itis.springfirstproject.models.State;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

@Component
public class SignUpServiceImpl implements SignUpService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ExecutorService threadPool;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void signUp(SignUpDto form) {
        String rawPassword = form.getPassword();
        String hashPassword = passwordEncoder.encode(rawPassword);

        User user = User.builder()
                .email(form.getEmail())
                .hashPassword(hashPassword)
                .name(form.getName())
                .number(form.getNumber())
                .createdAt(LocalDateTime.now())
                .state(State.NOT_CONFIRMED)
                .role(Role.USER)
                .rating(0)
                .confirmCode(UUID.randomUUID().toString())
                .build();

        usersRepository.save(user);
        threadPool.submit(() -> emailService.sendMail("Регистрация", "<a href=http://localhost/verification?confirm_code="+user.getConfirmCode()+">Подтвердить e-mail</a>", user.getEmail()));

    }
}

