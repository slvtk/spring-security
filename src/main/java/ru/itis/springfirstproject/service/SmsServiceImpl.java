package ru.itis.springfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.itis.springfirstproject.models.User;

import java.util.concurrent.ExecutorService;

@Service
public class SmsServiceImpl implements SmsService {

    @Autowired
    private ExecutorService threadPool;

    private final RestTemplate restTemplate = new RestTemplate();

    @Value("${sms.aero.email}")
    private String smsAeroEmail;

    @Value("${sms.aero.api-key}")
    private String smsAeroApiKey;

    @Value("${sms.aero.from}")
    private String smsAeroFrom;

    @Value("${sms.aero.type}")
    private String smsAeroType;

    @Value("${sms.aero.url}")
    private String smsAeroUrl;

    @Override
    public void sendSms(User user, String text) {
        threadPool.submit(() -> {

            String request = smsAeroUrl + "?user="
                    + smsAeroEmail + "&password="
                    + smsAeroApiKey + "&to="
                    + user.getNumber() +
                    "&text=" + text
                    + "&from="
                    + smsAeroFrom + "&type="
                    + smsAeroType;

            System.out.println(request);
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(request, String.class);
            if (responseEntity.getBody().contains("accepted")) {
                System.out.println("Сработаллллллллллллллллллллллл");
                return true;
            } else {
                throw new IllegalArgumentException("Incorrect phone number");
            }
        });
    }
}
