package ru.itis.springfirstproject.service;

import ru.itis.springfirstproject.dto.UserDto;
import ru.itis.springfirstproject.models.User;

import java.util.List;

public interface UsersService {

    List<UserDto> getAllUsers();

    List<UserDto> getAllUsersOrderByRatingDesc();

    User getAdmin();

}

