package ru.itis.springfirstproject.service;

import org.springframework.security.core.Authentication;
import ru.itis.springfirstproject.dto.ReviewDto;

public interface ReviewService {

    void sendReview(ReviewDto form , Authentication authentication);

}
