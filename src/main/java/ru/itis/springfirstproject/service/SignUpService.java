package ru.itis.springfirstproject.service;

import ru.itis.springfirstproject.dto.SignUpDto;

public interface SignUpService {

    void signUp(SignUpDto form);

}

