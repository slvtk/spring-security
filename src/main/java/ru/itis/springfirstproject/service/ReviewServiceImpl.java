package ru.itis.springfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.itis.springfirstproject.dto.ReviewDto;
import ru.itis.springfirstproject.models.*;
import ru.itis.springfirstproject.repositories.ReviewRepository;
import ru.itis.springfirstproject.repositories.TestsRepository;
import ru.itis.springfirstproject.repositories.UsersRepository;
import ru.itis.springfirstproject.security.details.UserDetailsImpl;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class ReviewServiceImpl implements ReviewService{
    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    TestsRepository testsRepository;
    @Autowired
    UsersRepository usersRepository;
    @Override
    public void sendReview(ReviewDto form , Authentication authentication) {

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        User user=userDetails.getUser();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>"+form.getReview());
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>"+form.getTest());
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>"+user);
        Review review = Review.builder()
                .test(form.getTest())
                .user(usersRepository.getOne(userDetails.getUser().getId()))
                .createdAt(LocalDateTime.now())
                .review(form.getReview())
                .build();
        System.out.println(testsRepository.getOne(form.getTest().getId()));
        System.out.println(form.getReview());
        reviewRepository.save(review);
    }
}
