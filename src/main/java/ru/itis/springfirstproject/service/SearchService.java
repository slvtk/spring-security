package ru.itis.springfirstproject.service;

import ru.itis.springfirstproject.dto.TestsSearchResult;

public interface SearchService {
    TestsSearchResult searchProducts(String query, Integer page, Integer size);
}
