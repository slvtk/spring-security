package ru.itis.springfirstproject.service;

public interface EmailService {
    void sendMail(String subject, String text, String email);
}

