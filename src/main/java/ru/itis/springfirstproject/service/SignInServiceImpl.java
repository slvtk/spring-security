package ru.itis.springfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.springfirstproject.dto.SignInDto;
import ru.itis.springfirstproject.models.CookieValue;
import ru.itis.springfirstproject.models.User;
import ru.itis.springfirstproject.repositories.UsersRepository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class SignInServiceImpl implements SignInService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Optional<String> signIn(SignInDto signInDto) {
        Optional<User> userOptional = usersRepository.findByEmail(signInDto.getEmail());
        String rawPassword = signInDto.getPassword();
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            String hashPassword = user.getHashPassword();
            if (passwordEncoder.matches(rawPassword, hashPassword)) {
                String newCookie = UUID.randomUUID().toString();

                CookieValue cookieValue = CookieValue.builder()
                        .value(newCookie)
                        .createdAt(LocalDateTime.now())
                        .user(user)
                        .build();

                return Optional.of(newCookie);
            } else {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }
}

