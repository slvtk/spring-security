package ru.itis.springfirstproject.service;

import ru.itis.springfirstproject.dto.SignInDto;

import java.util.Optional;

public interface SignInService {

    Optional<String> signIn(SignInDto signInDto);

}


