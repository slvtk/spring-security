package ru.itis.springfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.itis.springfirstproject.dto.TestsSearchResult;
import ru.itis.springfirstproject.models.Test;
import ru.itis.springfirstproject.repositories.TestsRepository;

@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    TestsRepository testsRepository;
    @Override
    public TestsSearchResult searchProducts(String query, Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page, size);
        Page<Test> pageResult = testsRepository.search(query, pageRequest);
        return TestsSearchResult.builder()
                .pagesCount(pageResult.getTotalPages())
                .tests(pageResult.getContent())
                .build();

    }
}
