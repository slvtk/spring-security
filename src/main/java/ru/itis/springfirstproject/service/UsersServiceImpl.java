package ru.itis.springfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ru.itis.springfirstproject.dto.UserDto;
import ru.itis.springfirstproject.models.Role;
import ru.itis.springfirstproject.models.User;
import ru.itis.springfirstproject.repositories.UsersRepository;

import java.util.List;
import java.util.Optional;

@Component
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

//    @Autowired
//    @Qualifier(value = "costsInputStream")
//    private InputStream inputStream;

    @Override
    public List<UserDto> getAllUsers() {
        List<User> users = usersRepository.findAll();
        return UserDto.from(users);
    }

    @Override
    public List<UserDto> getAllUsersOrderByRatingDesc() {
            List<User> users = usersRepository.findAll(Sort.by(Sort.Direction.DESC,"rating"));
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+users);
            return UserDto.from(users);
    }

    @Override
    public User getAdmin() {
        Optional<User> userOptional = usersRepository.findByRole(Role.ADMIN);
        return userOptional.orElse(null);
    }

}

