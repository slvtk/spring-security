package ru.itis.springfirstproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.springfirstproject.dto.TestDto;
import ru.itis.springfirstproject.models.Test;
import ru.itis.springfirstproject.repositories.TestsRepository;

import java.util.List;
import java.util.Optional;

@Component
public class TestsServiceImpl implements TestsService {

    @Autowired
    TestsRepository testsRepository;

    @Override
    public List<TestDto> getAllTests() {
        List<Test> tests = testsRepository.findAll();
        return TestDto.from(tests);
    }

    @Override
    public Test getTestById(Long id) {
        Test test=null;
        Optional<Test> testOptional = testsRepository.findById(id);
        if (testOptional.isPresent()) {
            test = testOptional.get();
        }
        return test;
    }

    @Override
    public Integer checkAnswers(Integer[] answers, Integer[] answersRight) {
        int i = 0;
        int result= 0;
        for (Integer answer :
                answers) {
            if (answer.equals(answersRight[i])){
                result+=1;
            }
            i++;
        }
        return result*25;
    }

}
