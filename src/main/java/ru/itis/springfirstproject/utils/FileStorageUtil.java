package ru.itis.springfirstproject.utils;

import lombok.SneakyThrows;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.springfirstproject.models.FileInfo;
import ru.itis.springfirstproject.repositories.FileInfoRepository;
import ru.itis.springfirstproject.repositories.UsersRepository;
import ru.itis.springfirstproject.security.details.UserDetailsImpl;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Component
public class FileStorageUtil {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private FileInfoRepository fileInfoRepository;

    @Value("${storage.path}")
    private String storagePath;

    public String getStoragePath() {
        return storagePath;
    }

    // сохраняет файл на диск
    @SneakyThrows
    public void copyToStorage(MultipartFile file, String storageFileName) {
        Files.copy(file.getInputStream(), Paths.get(storagePath, storageFileName));
    }

    // принимает на вход файл в формате Multipart
    // сохраняет его в БД
    public FileInfo convertFromMultipart(MultipartFile file, Authentication authentication) {
        // получаем название файла
        String originalFileName = file.getOriginalFilename();
        // вытаскиваем контент-тайп (MIME)
        String type = file.getContentType();
        // размер файла
        long size = file.getSize();
        // создаем имя файла
        String storageName = createStorageName(originalFileName);
        // получаем url файла по которому он будет доступен внутри системы
        String fileUrl = getUrlOfFile(storageName);


        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+userDetails.getUser());
         //get logged in username

        return FileInfo.builder()
                .originalFileName(originalFileName)
                .storageFileName(storageName)
                .url(fileUrl)
                .user(usersRepository.getOne(userDetails.getUser().getId()))
                .size(size)
                .type(type)
                .build();
    }

    private String getUrlOfFile(String storageFileName) {
        // путь к папке с файлами на диске + название файла
        return storagePath + "/" + storageFileName;
    }

    // создает уникальное имя файла на диске с его расширением
    private String createStorageName(String originalFileName) {
        // получаем расширение файла по его имени
        String extension = FilenameUtils.getExtension(originalFileName);
        // генерируем случайную строку
        String newFileName = UUID.randomUUID().toString();
        // новое имя файла - UUID + . + расширение файла
        return newFileName + "." + extension;
    }
}

