//package ru.itis.springfirstproject.controllers;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RestController;
//import ru.itis.springfirstproject.dto.UserDto;
//import ru.itis.springfirstproject.service.UsersService;
//
//import java.util.List;
//
//@RestController
//public class RatingController {
//
//    @Autowired
//    private UsersService usersService;
//
//    @GetMapping("/rating")
//    public ResponseEntity<List<UserDto>> ratingUsers(){
//
//        return ResponseEntity.ok(usersService.getAllUsersOrderByRatingDesc());
//
//    }
//}
