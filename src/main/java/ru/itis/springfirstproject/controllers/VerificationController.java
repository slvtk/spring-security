package ru.itis.springfirstproject.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import ru.itis.springfirstproject.models.State;
import ru.itis.springfirstproject.models.User;
import ru.itis.springfirstproject.repositories.UsersRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Controller

public class VerificationController {
    @Autowired
    private UsersRepository usersRepository;

    @GetMapping("/verification")
    public String verification(HttpServletRequest request) {
        System.out.println(request.getParameter("confirm_code"));
        Optional<User> userOptional = usersRepository.findByConfirmCode(request.getParameter("confirm_code"));
        System.out.println(userOptional.toString());
        if (userOptional.isPresent()) {
            User user=userOptional.get();
            System.out.println(user);
            System.out.println(user.getEmail());
            user.setState(State.CONFIRMED);
            System.out.println(user.getState());
            usersRepository.save(user);
        }
        return "redirect:/signIn";
    }
}
