package ru.itis.springfirstproject.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.springfirstproject.dto.SignInDto;
import ru.itis.springfirstproject.dto.SignUpDto;
import ru.itis.springfirstproject.models.User;
import ru.itis.springfirstproject.repositories.UsersRepository;
import ru.itis.springfirstproject.service.SignUpService;
import ru.itis.springfirstproject.service.SmsService;
import ru.itis.springfirstproject.service.UsersService;

import java.util.Optional;

@Controller
public class SignUpController {
    @Autowired
    private SignUpService service;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private SmsService smsService;

    @GetMapping("/signUp")
    public String getSignUpPage() {
        return "sign_up";
    }

    @PostMapping("/signUp")
    public String signUp(SignUpDto form) {
        service.signUp(form);
        smsService.sendSms(usersRepository.findByEmail(form.getEmail()).get(),"Hello");
        System.out.println(">>>>>>>>>>>>> Сработал");
        return "redirect:/signUp";
    }
}
