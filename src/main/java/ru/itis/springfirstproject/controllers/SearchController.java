package ru.itis.springfirstproject.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.springfirstproject.service.SearchService;

@Controller
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @GetMapping("/products")
    public String searchProducts(Model model, @RequestParam("q") String query,
                                                   @RequestParam("page") Integer page,
                                                   @RequestParam("size") Integer size) {
        model.addAttribute("tests", searchService.searchProducts(query, page, size).getTests());
        return "search";
    }
}
