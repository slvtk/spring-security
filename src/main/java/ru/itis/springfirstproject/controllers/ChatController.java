package ru.itis.springfirstproject.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.itis.springfirstproject.models.User;
import ru.itis.springfirstproject.security.details.UserDetailsImpl;
import ru.itis.springfirstproject.service.MessageService;
import ru.itis.springfirstproject.service.UsersService;

@Controller
public class ChatController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private UsersService usersService;

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/support")
    public String getIndexPage(Model model, Authentication authentication) {
        if (authentication != null) {
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            User user = userDetails.getUser();
            model.addAttribute("user", user);
            model.addAttribute("admin", usersService.getAdmin().getEmail());
            model.addAttribute("messages", messageService.getDialogue(user.getEmail(),usersService.getAdmin().getEmail()));
        }
        return "support";
    }
}