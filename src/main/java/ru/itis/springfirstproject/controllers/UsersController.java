package ru.itis.springfirstproject.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.itis.springfirstproject.dto.UserDto;
import ru.itis.springfirstproject.models.User;
import ru.itis.springfirstproject.service.UsersService;
import java.util.List;

@Controller
public class UsersController {

    @Autowired
    private UsersService usersService;

    @GetMapping("/users")
    public ResponseEntity<List<UserDto>> getUsersPage() {
        return ResponseEntity.ok(usersService.getAllUsers());
    }
}
