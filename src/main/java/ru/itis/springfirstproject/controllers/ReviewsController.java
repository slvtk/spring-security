package ru.itis.springfirstproject.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itis.springfirstproject.dto.ReviewDto;
import ru.itis.springfirstproject.models.Review;
import ru.itis.springfirstproject.repositories.ReviewRepository;
import ru.itis.springfirstproject.service.ReviewService;

import java.util.List;

@Controller
@RequestMapping("/reviews")
public class ReviewsController {
    @Autowired
    ReviewService reviewService;
    @Autowired
    ReviewRepository reviewRepository;

    @GetMapping()
    public ResponseEntity<List<Review>> getAllReviews(Model model){
        return ResponseEntity.ok(reviewRepository.findAll());
    }
    @GetMapping("/new")
    public String getNewReview(){
        return "review_new";
    }

    @PostMapping()
    public String postReview(ReviewDto form , Authentication authentication){
        reviewService.sendReview(form , authentication);
        return "redirect:/reviews";
    }
}
