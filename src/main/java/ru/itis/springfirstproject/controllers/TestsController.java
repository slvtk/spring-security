package ru.itis.springfirstproject.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.springfirstproject.dto.TestDto;
import ru.itis.springfirstproject.models.FileInfo;
import ru.itis.springfirstproject.models.Question;
import ru.itis.springfirstproject.models.Test;
import ru.itis.springfirstproject.models.User;
import ru.itis.springfirstproject.repositories.QuestionsRepository;
import ru.itis.springfirstproject.repositories.UsersRepository;
import ru.itis.springfirstproject.security.details.UserDetailsImpl;
import ru.itis.springfirstproject.service.TestsService;
import ru.itis.springfirstproject.service.UsersService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class TestsController {

    @Autowired
    TestsService testsService;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    UsersService usersService;

    @Autowired
    QuestionsRepository questionsRepository;

    @GetMapping("/tests")
    public String testsList(Model model) {

        List<TestDto> tests = testsService.getAllTests();
        model.addAttribute("tests", tests);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>." + tests);
        return "tests";
    }

    @GetMapping("/tests/{id:.+}")
    public String getTest(@PathVariable("id") Long id, Model model) {
        Test test = testsService.getTestById(id);
        List<Question> questions = questionsRepository.findAllByTest(test);
        System.out.println(">>>>>>>>>>>>>>>>>>" + test);
        System.out.println(">>>>>>>>>>>>>>>>>>" + questions);
        model.addAttribute("questions", questions);
        model.addAttribute("test", test);
        return "test";
    }

    @PostMapping("/tests")
    public String postTest(HttpServletRequest request, Authentication authentication) {
        Test test = testsService.getTestById(Long.parseLong(request.getParameter("test")));
        List<Question> questions = questionsRepository.findAllByTest(test);
        //это лист хранящий верные ответы
        List<Integer> answersRightList = new ArrayList<>();
        for (Question question :
                questions) {
            answersRightList.add(Integer.parseInt(question.getAnswer_correct()));
        }
        //тут лежат ответы из формы пришедшие в параметрах
        Integer[] answers = Arrays.stream(Arrays.stream(request.getParameterValues("answer")).mapToInt(Integer::parseInt).toArray()).boxed().toArray(Integer[]::new);
        //тут лежат верные ответы
        Integer[] answersRight = answersRightList.toArray(new Integer[0]);
        Integer countRating = testsService.checkAnswers(answers, answersRight);


        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        User user = userDetails.getUser();
        user.setRating(user.getRating() + countRating);
        usersRepository.save(user);

        return "redirect:/profile";
    }
}
