package ru.itis.springfirstproject.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.springfirstproject.models.User;
import ru.itis.springfirstproject.repositories.FileInfoRepository;
import ru.itis.springfirstproject.security.details.UserDetailsImpl;

@RestController
public class ProfileController {

    @Autowired
    FileInfoRepository fileInfoRepository;

@GetMapping("/profile")
    public ResponseEntity<User> getProfilePage(Authentication authentication, Model model) {

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return ResponseEntity.ok(userDetails.getUser());
    }
}