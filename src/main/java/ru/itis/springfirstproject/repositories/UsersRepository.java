package ru.itis.springfirstproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springfirstproject.models.Role;
import ru.itis.springfirstproject.models.User;

import java.util.Optional;


public interface UsersRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    Optional<User> findByConfirmCode(String confirmCode);

    Optional<User> findByRole(Role role);
}

