package ru.itis.springfirstproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springfirstproject.models.FileInfo;
import ru.itis.springfirstproject.models.User;

public interface FileInfoRepository extends JpaRepository<FileInfo, Long> {

    FileInfo findOneByStorageFileName(String storageFileName);

    FileInfo findByUser(User user);

}

