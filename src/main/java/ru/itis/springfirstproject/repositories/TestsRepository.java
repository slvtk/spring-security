package ru.itis.springfirstproject.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.springfirstproject.models.Test;

import java.util.Optional;

public interface TestsRepository extends JpaRepository<Test, Long> {

    Optional<Test> findById(Long id);

    @Query("from Test test where " +
            "(upper(test.title) like concat('%', upper(:query), '%') or " +
            "upper(test.description) like concat('%', upper(:query), '%')) ")
    Page<Test> search(@Param("query") String query,
                      Pageable pageable);

}
