package ru.itis.springfirstproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springfirstproject.models.Question;
import ru.itis.springfirstproject.models.Test;

import java.util.List;

public interface QuestionsRepository extends JpaRepository<Question, Long> {

    List<Question> findAllByTest(Test test);

}
