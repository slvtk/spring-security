package ru.itis.springfirstproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springfirstproject.models.Review;

public interface ReviewRepository extends JpaRepository<Review,Long> {
}
